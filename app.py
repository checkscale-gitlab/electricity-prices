from dotenv import dotenv_values
import requests
import json
import sys
import telegram
import argparse
import os

# Check if env variables are setted
try:
    ESIOS_TOKEN = os.environ["ESIOS_TOKEN"]
    TELEGRAM_TOKEN = os.environ["TELEGRAM_TOKEN"]
    TELEGRAM_CHAT_ID = os.environ["TELEGRAM_CHAT_ID"]
except:
    #Load .env
    config = dotenv_values(".env")

    ESIOS_TOKEN = config["ESIOS_TOKEN"]
    TELEGRAM_TOKEN = config["TELEGRAM_TOKEN"]
    TELEGRAM_CHAT_ID = config["TELEGRAM_CHAT_ID"]

parser = argparse.ArgumentParser()

parser.add_argument("-hours", "--num_hours", dest="num_hours",
                    default=24, help="Max num of hours")
parser.add_argument("-start", "--start_hour", dest="start_hour",
                    default=-1, help="Start at specific hour")
parser.add_argument("-order", "--order", dest="order",
                    default="hour", help="Start at specific hour")

args = parser.parse_args()

order = {"hour": 0, "price": 1}


def tg_notify(best_hours, worst_hours, chat_id, token, num_hours, error):
    if error == None:
        msg = "Best hours:\n"
        # print(best_hours)
        for item in best_hours:
            msg += item[0]+"h ("+str(item[1])+" €)\n"
        if num_hours != 24:
            msg += "------------\n"
            msg += "Worst hours:\n"
            for item in worst_hours:
                msg += item[0]+"h ("+str(item[1])+" €)\n"
    else:
        msg = "There was an error getting electricity prices:\n"
        msg += str(error)
    bot = telegram.Bot(token=token)
    bot.sendMessage(chat_id=chat_id, text=msg)


def average_price(prices_by_hour):
    average = 0
    for price in prices_by_hour.values():
        average += price
    return format(float(average/len(prices_by_hour)), '.5f')


url = 'https://api.esios.ree.es/archives/70/download_json'
headers = {'Accept': 'application/json; application/vnd.esios-api-v2+json', 'Content-Type': 'application/json',
           'Host': 'api.esios.ree.es', 'Authorization': 'Token token=' + ESIOS_TOKEN}
response = requests.get(url, headers=headers)
if response.status_code == 200:
    try:
        json_data = json.loads(response.text)
        values = json_data['PVPC']
        prices_by_hour = {}
        for value in values:
            hour = value["Hora"]
            price = float(value['PCB'].replace(",", "."))
            prices_by_hour[hour] = round(price/1000, 4)

        sorted_prices = sorted(prices_by_hour.items(), key=lambda x: x[1])
        average = average_price(prices_by_hour)
        # Save best/worst "num_hours" defined by user
        best_hours = {}
        worst_hours = {}
        i = 1
        for elem in sorted_prices:
            if elem[1] >= float(average):
                continue
            first_hour = elem[0].split('-')
            first_hour = int(first_hour[0])
            if first_hour >= int(args.start_hour):
                best_hours[elem[0]] = elem[1]
                i += 1
                if i > int(args.num_hours):
                    break
        i = 1
        for elem in reversed(sorted_prices):
            if elem[1] < float(average):
                continue
            first_hour = elem[0].split('-')
            first_hour = int(first_hour[0])
            if first_hour >= int(args.start_hour):
                worst_hours[elem[0]] = elem[1]
                i += 1
                if i > int(args.num_hours):
                    break

        # Sort the best/worst hours according to user selection
        best_hours = sorted(best_hours.items(),
                            key=lambda x: x[order[args.order]])
        worst_hours = sorted(worst_hours.items(),
                             key=lambda x: x[order[args.order]])

        print("########################")
        print("###### Best hours ######")
        print("########################")
        for item in best_hours:
            print(item[0]+"h ("+str(item[1])+" €)")

        print("#########################")
        print("###### Worst hours ######")
        print("#########################")
        for item in worst_hours:
            print(item[0]+"h ("+str(item[1])+" €)")

        min_hour = best_hours[0][0]+"h ("+str(best_hours[0][1])+" €)"
        max_hour = worst_hours[0][0]+"h ("+str(worst_hours[0][1])+" €)"

        print("#########################")
        print("Avegare price: %s €" % average)

        tg_notify(best_hours, worst_hours, TELEGRAM_CHAT_ID, TELEGRAM_TOKEN,
                  args.num_hours, None)
    except Exception as e:
        print("Error getting prices:", e)
        tg_notify(best_hours, worst_hours, TELEGRAM_CHAT_ID, TELEGRAM_TOKEN,
                  args.num_hours, e)
