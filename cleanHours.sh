#!/bin/bash
    
input="besthours.txt"
while IFS= read -r line
do
if [[ $line == *"Worst"* ]]; then
    break
fi
if [[ $line == *"-"* ]]; then
    hours=($(echo $line | tr "-" "\n"))
    hour=${hours[0]}
    list_hours=$list_hours","$hour
fi
done < "$input"
echo "${list_hours:1}" > customCron.txt